package com.awsdynamodb.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.awsdynamodb.command.AddStudentCommand;
import com.awsdynamodb.command.EditStudentCommand;
import com.awsdynamodb.command.SearchStudentCommand;
import com.awsdynamodb.command.ViewAllStudentDetailsCommand;
import com.awsdynamodb.entity.Customer;
import com.awsdynamodb.request.EditStudentDetailsRequest;
import com.awsdynamodb.request.StudentDetailsRequest;
import com.awsdynamodb.response.Response;
import com.awsdynamodb.response.ViewStudentDetailsResponse;

import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping(value = "/v1")
@CrossOrigin(origins = "*")
public class StudentController {

	private AddStudentCommand addStudentCommand;
	private ViewAllStudentDetailsCommand viewAllStudentDetailsCommand;
	private EditStudentCommand editStudentCommand;
	private SearchStudentCommand searchStudentCommand;

	@Autowired
	public StudentController(AddStudentCommand addStudentCommand,
			ViewAllStudentDetailsCommand viewAllStudentDetailsCommand, EditStudentCommand editStudentCommand,
			SearchStudentCommand searchStudentCommand) {
		this.addStudentCommand = addStudentCommand;
		this.viewAllStudentDetailsCommand = viewAllStudentDetailsCommand;
		this.editStudentCommand = editStudentCommand;
		this.searchStudentCommand = searchStudentCommand;
	}

	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_OK, response = Response.class, message = "Student Added Successfully"),
			@ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, response = String.class, message = "Invalid parameters"),
			@ApiResponse(code = HttpServletResponse.SC_UNAUTHORIZED, response = String.class, message = "Invalid Token / Without Token"),
			@ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, response = String.class, message = "UnAuthorized Access") })
	@PostMapping(value = "/addStudent", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response> addStudentInformation(@RequestBody StudentDetailsRequest studentDetailsRequest) {
		return ResponseEntity.status(HttpStatus.OK).body(addStudentCommand.execute(studentDetailsRequest));
	}

	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_OK, response = Response.class, message = "All Student Details fetched Successfully"),
			@ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, response = String.class, message = "Invalid parameters"),
			@ApiResponse(code = HttpServletResponse.SC_UNAUTHORIZED, response = String.class, message = "Invalid Token / Without Token"),
			@ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, response = String.class, message = "UnAuthorized Access") })
	@GetMapping(value = "/viewAllStudentDetails", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ViewStudentDetailsResponse> viewAllStudentsDetails() {
		return viewAllStudentDetailsCommand.execute();
	}

	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_OK, response = Response.class, message = "Student Details Updated Successfully"),
			@ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, response = String.class, message = "Invalid parameters"),
			@ApiResponse(code = HttpServletResponse.SC_UNAUTHORIZED, response = String.class, message = "Invalid Token / Without Token"),
			@ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, response = String.class, message = "UnAuthorized Access") })
	@PutMapping(value = "/editStudentDetails", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response> editStudentInformation(
			@RequestBody EditStudentDetailsRequest editStudentDetailsRequest) {
		return ResponseEntity.status(HttpStatus.OK).body(editStudentCommand.execute(editStudentDetailsRequest));
	}

	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_OK, response = Response.class, message = "All Student Details fetched Successfully"),
			@ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, response = String.class, message = "Invalid parameters"),
			@ApiResponse(code = HttpServletResponse.SC_UNAUTHORIZED, response = String.class, message = "Invalid Token / Without Token"),
			@ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, response = String.class, message = "UnAuthorized Access") })
	@GetMapping(value = "/searchStudent/{name}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Customer>> searchStudentByName(@PathVariable String name) {
		return ResponseEntity.status(HttpStatus.OK).body(searchStudentCommand.execute(name));
	}

}
