package com.awsdynamodb.repository;

import java.util.List;
import java.util.Optional;

import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;

import com.awsdynamodb.entity.Customer;

@EnableScan
public interface StudentRepository extends CrudRepository<Customer, String> {

	List<Customer> findByName(String studentName);

	Optional<Customer> findById(String string);

}
