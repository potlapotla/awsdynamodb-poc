package com.awsdynamodb.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.awsdynamodb.common.Constants;
import com.awsdynamodb.entity.Customer;
import com.awsdynamodb.exception.DataNotFoundException;
import com.awsdynamodb.repository.StudentRepository;
import com.awsdynamodb.request.EditStudentDetailsRequest;
import com.awsdynamodb.request.StudentDetailsRequest;
import com.awsdynamodb.response.Response;
import com.awsdynamodb.response.ViewStudentDetailsResponse;

@Service
public class StudentInformationService {

	@Autowired
	StudentRepository studentRepository;

	public Response addStudentInformation(StudentDetailsRequest studentDetailsRequest) {
		Customer student = new Customer();
		student.setId(studentDetailsRequest.getId());
		student.setName(studentDetailsRequest.getName());
		student.setAddress1(studentDetailsRequest.getAddress1());
		student.setAddress2(studentDetailsRequest.getAddress2());
		student.setBranch(studentDetailsRequest.getBranch());
		student.setCity(studentDetailsRequest.getCity());
		student.setEmailId(studentDetailsRequest.getEmailId());
		student.setFatherName(studentDetailsRequest.getFatherName());
		student.setPhoneNumber(studentDetailsRequest.getPhoneNumber());
		student.setRollNumber(studentDetailsRequest.getRollNumber());
		student.setState(studentDetailsRequest.getState());
		studentRepository.save(student);
		Response response = new Response();
		response.setId(student.getId());
		response.setStatus(Constants.SUCCESS);
		response.setMessage(Constants.STUDENT_DATA_ADDED_SUCCESFULLY);
		return response;
	}

	public ViewStudentDetailsResponse viewAllStudentDetails() {
		List<Customer> studentList = (List<Customer>) studentRepository.findAll();

		if (studentList.isEmpty()) {
			throw new DataNotFoundException(Constants.List_IsEmpty + studentList.size());
		}

		ViewStudentDetailsResponse viewStudentDetailsResponse = new ViewStudentDetailsResponse();
		viewStudentDetailsResponse.setData(studentList);
		viewStudentDetailsResponse.setMessage(Constants.ALL_STUDENTS_DATA_FETCHED_SUCCESSFULLY);
		viewStudentDetailsResponse.setStatus(Constants.SUCCESS);
		viewStudentDetailsResponse.setTotal(studentList.size());
		return viewStudentDetailsResponse;
	}

	public Response editStudentInformation(EditStudentDetailsRequest editStudentDetailsRequest) {
		Optional<Customer> student = studentRepository.findById(editStudentDetailsRequest.getStudentId());

		if (!student.isPresent()) {
			throw new DataNotFoundException(
					Constants.STUDENT_NOT_FOUND_WITH_ID + editStudentDetailsRequest.getStudentId());
		}

		Customer std = student.get();
		std.setAddress1(editStudentDetailsRequest.getAddress1());
		std.setAddress2(editStudentDetailsRequest.getAddress2());
		std.setCity(editStudentDetailsRequest.getCity());
		std.setEmailId(editStudentDetailsRequest.getEmailId());
		std.setState(editStudentDetailsRequest.getState());
		studentRepository.save(std);
		Response response = new Response();
		response.setId(std.getId());
		response.setStatus(Constants.SUCCESS);
		response.setMessage(Constants.STUDENT_DATA_UPDATED_SUCCESFULLY);
		return response;
	}

	public List<Customer> searchStudentInformationByName(String studentName) {
		List<Customer> studentList = studentRepository.findByName(studentName);
		if (studentList.isEmpty()) {
			throw new DataNotFoundException(Constants.STUDENT_NOT_FOUND_WITH_SEARCHED_NAME);
		}
		return studentList;
	}

}
