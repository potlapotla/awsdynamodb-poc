package com.awsdynamodb.response;

import java.util.List;

import com.awsdynamodb.entity.Customer;

public class ViewStudentDetailsResponse extends Response {
	private String status;
	private List<Customer> data;
	private int total;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<Customer> getData() {
		return data;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public void setData(List<Customer> data) {
		this.data = data;
	}

}
