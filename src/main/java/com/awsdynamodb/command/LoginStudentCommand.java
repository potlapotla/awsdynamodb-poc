package com.awsdynamodb.command;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.awsdynamodb.request.LoginStudentRequest;
import com.awsdynamodb.response.Response;
import com.awsdynamodb.service.StudentInformationService;


@Service
public class LoginStudentCommand implements Command<LoginStudentRequest, ResponseEntity<Response>> {

	@Autowired
	StudentInformationService studentInformationService;

	@Override
	public ResponseEntity<Response> execute(LoginStudentRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * @Override public ResponseEntity<Response> execute(LoginStudentRequest
	 * request) { return
	 * ResponseEntity.status(HttpStatus.OK).body(studentInformationService.
	 * loginStudent(request)); }
	 */

}
