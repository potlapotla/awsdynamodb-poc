package com.awsdynamodb.command;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.awsdynamodb.entity.Customer;
import com.awsdynamodb.service.StudentInformationService;


@Service
public class SearchStudentCommand implements Command<String, List<Customer>>{

	@Autowired
	StudentInformationService studentInformationService;
	
	@Override
	public List<Customer> execute(String studentName) {
		return studentInformationService.searchStudentInformationByName(studentName);
	}

}
