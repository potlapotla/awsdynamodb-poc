package com.awsdynamodb.command;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.awsdynamodb.request.StudentDetailsRequest;
import com.awsdynamodb.response.Response;
import com.awsdynamodb.service.StudentInformationService;

@Service
public class AddStudentCommand implements Command<StudentDetailsRequest, Response> {

	@Autowired
	StudentInformationService studentInformationService;

	@Override
	public Response execute(StudentDetailsRequest request) {

		return studentInformationService.addStudentInformation(request);
	}


}
