package com.awsdynamodb.command;

public interface ViewStudentsCommand <T>{
	public T execute();
}
