package com.awsdynamodb.command;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.awsdynamodb.request.EditStudentDetailsRequest;
import com.awsdynamodb.response.Response;
import com.awsdynamodb.service.StudentInformationService;

@Service
public class EditStudentCommand implements Command<EditStudentDetailsRequest, Response> {

	@Autowired
	StudentInformationService studentInformationService;

	@Override
	public Response execute(EditStudentDetailsRequest request) {
		return studentInformationService.editStudentInformation(request);
	}

}
