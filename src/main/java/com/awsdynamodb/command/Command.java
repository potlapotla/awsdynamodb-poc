package com.awsdynamodb.command;

public interface Command <E, T> {
	
	public T execute(E request);
	
	
}
