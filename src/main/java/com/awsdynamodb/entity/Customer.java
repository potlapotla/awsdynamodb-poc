package com.awsdynamodb.entity;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

@DynamoDBTable(tableName = "Customer")
public class Customer {

	private String id;
	private String name;
	private String emailId;
	private String phoneNumber;
	private String branch;
	private int rollNumber;
	private String fatherName;
	private String address1;
	private String address2;
	private String city;
	private String state;

	public Customer() {
	}

	@DynamoDBHashKey(attributeName = "Id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	//@DynamoDBHashKey(attributeName = "Name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	//@DynamoDBHashKey(attributeName = "EmailId")
	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	//@DynamoDBHashKey(attributeName = "PhoneNumber")
	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	//@DynamoDBHashKey(attributeName = "Branch")
	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	//@DynamoDBHashKey(attributeName = "RollNumber")
	public int getRollNumber() {
		return rollNumber;
	}

	public void setRollNumber(int rollNumber) {
		this.rollNumber = rollNumber;
	}

	//@DynamoDBHashKey(attributeName = "FatherName")
	public String getFatherName() {
		return fatherName;
	}

	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}

	//@DynamoDBHashKey(attributeName = "Address1")
	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	//@DynamoDBHashKey(attributeName = "Address2")
	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	//@DynamoDBHashKey(attributeName = "City")
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	//@DynamoDBHashKey(attributeName = "State")
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

}
